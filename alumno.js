function Alumno(nc, n, ap, am, g){
  var _ncontrol = nc;
  var _nombre = n;
  var _apellidoP = ap;
  var _apellidoM = am;
  var _genero = g;
  
  function _getnControl(){
    return _ncontrol;
  }
  
  function _setnControl(nc){
    _ncontrol = nc;
  }

  function _getNombre(){
    return _nombre;
  }
  
  function _setNombre(n){
    _nombre = n;
  }
  
  function _getApellidoP(){
    return _apellidoP;
  }
  
  function _setApellidoP(ap){
    _apellidoP = ap;
  }
  
  function _getApellidoM(){
    return _apellidoM;
  }
  
  function _setApellidoM(am){
    _apellidoM = am;
  }
  
  function _getGenero(){
    return _genero;
  }
  
  function _setGenero(g){
    _genero = g;
  }
  
  return{
    "getnControl": _getnControl,
    "getNombre": _getNombre,
    "getApellidoP": _getApellidoP,
    "getApellidoM": _getApellidoM,
    "getGenero": _getGenero
  };
}
/*
var obj1 = Alumno(1234,'Lily','Santiago', 'Lopez','M');
var obj2 = Alumno(1345,'Lalo','Velasquez','Zurita','H');
var obj3 = Alumno(1456,'Juan','Hernandez','Merino','H');
var obj4 = Alumno(1567,'Lola','Vera','Sanchez','M');
var obj5 = Alumno(1678,'Jose','Martinez','Ruiz','H');

var gpo1 = Grupo(111,"isc");



gpo1.agregar(obj1);
gpo1.listar();
*/
