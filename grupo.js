function Grupo(c, n){
  var _clave = c;
  var _nombre = n;
  var _lista = [];
  
  function _getClave(){
    return _clave;
  }
  
  function _setClave(c){
    _clave = c;
  }

  function _getNombre(){
    return _nombre;
  }
  
  function _setNombre(n){
    _nombre = n;
  }
  
  function _agregar(a){
    if(_existe(a.getnControl()))
//      _lista.push(a);
      console.log("El alumno ya existe");
    else
//      console.log("El alumno ya existe");
      _lista.push(a);
  }
  
  function _borrar(a){
   if(_existe(a.getnControl()))
      _lista.splice(i, 1);
    else
      console.log("No existe Alumno...");  
  }
  
  function _editar(nc,n,ap,am,g){
   var i = _buscar(nc);
   _lista[i].setnControl(nc);
   _lista[i].setNombre(n);
   _lista[i].setApellidoP(ap);
   _lista[i].setApellidoM(am);
   _lista[i].setGenero(g); 
  }
  
  function _buscar(nc){ 
    for(var i = 0;i< _lista.length; i++){
      if(_lista[i].getnControl() === nc)
        return i;
      else
        console.log("No encontrado...");
    } 
  }
  
  function _existe(nc){  
    for(var i = 0;i< _lista.length; i++){
      if(_lista[i].getnControl() === nc)
        return true;
    }
    return false;
  }
  
  function _listar(){
    for(var i = 0;i< _lista.length; i++)
      console.log("Numero de control:"+_lista[i].getnControl()+" Nombre:"+_lista[i].getNombre() +" ApellidoP:"+_lista[i].getApellidoP()+" ApellidoM:"+_lista[i].getApellidoM()+" Genero:"+_lista[i].getGenero());
  }
   
  return{
    "agregar": _agregar,
    "borrar": _borrar,
    "editar": _editar,
    "buscar": _buscar,
    "existe": _existe,
    "listar": _listar,
  };
}
